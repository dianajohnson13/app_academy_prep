# Write a method that takes in a string. Your method should return the
# most common letter in the array, and a count of how many times it
# appears.
#
# Difficulty: medium.

def most_common_letter(string)
  all = string.chars
  letters = all.uniq
  i = 0
  high_letter = ""
  high_value = 0
  while i < letters.length
    current_letter = letters[i]
    current_value = all.count(current_letter)
    if current_value > high_value
      high_value = current_value
      high_letter = current_letter
    end
    i += 1
  end
  return [high_letter, high_value]
end

puts most_common_letter("abda")


# These are tests to check that your code is working. After writing
# your solution, they should all print true.

puts(
  'most_common_letter("abca") == ["a", 2]: ' +
  (most_common_letter('abca') == ['a', 2]).to_s
)
puts(
  'most_common_letter("abbab") == ["b", 3]: ' +
  (most_common_letter('abbab') == ['b', 3]).to_s
)
