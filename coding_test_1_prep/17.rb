# Write a method that takes in a string of lowercase letters (no
# uppercase letters, no repeats). Consider the *substrings* of the
# string: consecutive sequences of letters contained inside the string.
# Find the longest such string of letters that is a palindrome.
#
# Note that the entire string may itself be a palindrome.
#
# You may want to use Array's `slice(start_index, length)` method,
# which returns a substring of length `length` starting at index
# `start_index`:
#
#     "abcd".slice(1, 2) == "bc"
#     "abcd".slice(1, 3) == "bcd"
#     "abcd".slice(2, 1) == "c"
#     "abcd".slice(2, 2) == "cd"
#
# Difficulty: hard.

def palindrome?(string)
  i = 0
  while i < (string.length / 2) 
    if string[i] != string[(string.length - 1) - i]
      return false
    end

    i += 1
  end

  return true
end

def longest_palindrome(string)
  idx1 = 0
  longest_p = nil
  lolp = 0  #variable for length of longest_p
  while idx1 < string.length
  idx2 = idx1 + 1
    while idx2 <= string.length
      potential_p = string.slice(idx1, (idx2 - idx1))
      #puts potential_p
      if (palindrome?(potential_p) == true) && (potential_p.length > lolp)
        longest_p = potential_p
        lolp = potential_p.length
      end
      idx2 += 1
    end
    idx1 += 1
  end
  return longest_p
end

# These are tests to check that your code is working. After writing
# your solution, they should all print true.


puts(
  'longest_palindrome("abcbd") == "bcb": ' +
  (longest_palindrome('abcbd') == 'bcb').to_s
)
puts(
  'longest_palindrome("abba") == "abba": ' +
  (longest_palindrome('abba') == 'abba').to_s
)
puts(
  'longest_palindrome("abcbdeffe") == "effe": ' +
  (longest_palindrome('abcbdeffe') == 'effe').to_s
)
