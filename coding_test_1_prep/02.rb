def longest_word(sentence)
  words = sentence.split
  lolw = 0
  lw = ""
  i = 0
  while i < words.length
    current_word = words[i]
    locw = current_word.length
    if locw > lolw
      lolw = locw
      lw = current_word
    end
    i += 1
  end
  return lw
end

puts('longest_word("penguins love candy") == "penguins": ' + (longest_word('penguins love candy') == 'penguins').to_s)

puts('longest_word("I love pie") == "love": ' + (longest_word('I love pie') == 'love').to_s)

puts('longest_word("short longest") == "longest": ' + (longest_word('short longest') == 'longest').to_s)

puts('longest_word("one") == "one": ' + (longest_word('one') == 'one').to_s)

puts('longest_word("abc def abcde") == "abcde": ' + (longest_word('abc def abcde') == 'abcde').to_s)