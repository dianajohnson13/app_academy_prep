# Write a method that takes an array of numbers in. Your method should
# return the third greatest number in the array. You may assume that
# the array has at least three numbers in it.
#
# Difficulty: medium.

def third_greatest(nums)
  if nums.length >= 2
    i = nums.length - 3
    nums.sort!
    return nums[i]
  end
end #sort method not super efficient for computer

def third_greatest(nums)
  first = nil
  second = nil
  third = nil
  i = 0
  while i < nums.length
  x = nums[i]
    if first == nil || x > first
      third = second
      second = first
      first = x
    elsif second == nil || x > second
      third = second
      second = x
    elsif third == nil || x > third
    third = x
    end
  i += 1
  end
  return third
end



# These are tests to check that your code is working. After writing
# your solution, they should all print true.


puts(
  'third_greatest([5, 3, 7]) == 3: ' +
  (third_greatest([5, 3, 7]) == 3).to_s
)
puts(
  'third_greatest([5, 3, 7, 4]) == 4: ' +
  (third_greatest([5, 3, 7, 4]) == 4).to_s
)
puts(
  'third_greatest([2, 3, 7, 4]) == 3: ' +
  (third_greatest([2, 3, 7, 4]) == 3).to_s
)
