def no_repeats?(year)
	digits = year.to_s
	i = 0
	while i < digits.length - 1
		j = i + 1
		while j < digits.length
			return false if digits[i] == digits[j]
			j += 1
		end
		i += 1
	end
	return true
end	

def no_repeats(year_start, year_end)
	non_repeating_years = []
	(year_start..year_end).each do |year|
		non_repeating_years << year if no_repeats?(year)
	end
	return non_repeating_years
end