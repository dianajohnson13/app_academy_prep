
def bubble_sort(arr)
	sorted = false
	until sorted
		sorted = true
		(arr.count - 1).times do |idx|
			if arr[idx] > arr[idx + 1]
				arr[idx], arr[idx + 1] = arr[idx + 1], arr[idx]
				sorted = false
			end
		end
	end
	return arr
end