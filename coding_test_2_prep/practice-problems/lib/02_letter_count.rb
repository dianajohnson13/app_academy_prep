def letter_count(str)
	characters = (str.gsub(/[" "]/, "")).chars
	letters = Hash.new(0)
	characters.each {|character| letters[character] += 1}
	return letters
end