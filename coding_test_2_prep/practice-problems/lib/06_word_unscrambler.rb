def word_split_and_sort(word)
	word.chars.sort.join
end

def word_unscrambler(str, words)
	sorted_str = str.chars.sort.join
	matches = words.select{|word| word_split_and_sort(word) == sorted_str}
	return matches
end


puts word_unscrambler("pits", ["tips", "stip", "chug"])