def just_vowels(word)
	vowels = ["a", "e", "i", "o", "u"]
	letters = word.chars
	vowels_in_word = []
	letters.each {|letter| vowels_in_word << letter if (vowels.include?(letter))}
	return vowels_in_word
end

def vowels_ordered?(word)
	vowels = ["a", "e", "i", "o", "u"]
	vowels_in_word = just_vowels(word)
	index_of_previous_vowel = 0
	vowels_in_word.each do |letter|
		return false if vowels.index(letter) < index_of_previous_vowel
		index_of_previous_vowel = vowels.index(letter)
	end
	return true
end

def ordered_vowel_words(str)
	new_string = []
	words = str.split
	words.each {|word| new_string << word if vowels_ordered?(word)}
	return new_string.join" "
end
