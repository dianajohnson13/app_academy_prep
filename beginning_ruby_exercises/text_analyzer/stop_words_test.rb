text = %q{Los Angeles has some of the nicest weather in the country.}
stop_words = %w{the a by on for of are with just but and to my I has some in}

words = text.scan(/\w+/)
keywords = words.select {|word| !stop_words.include?(word)}
precent_non_stop_words = ((keywords.length.to_f / words.length.to_f) * 100).to_i

puts "Keywords: #{keywords.join " "}"
puts "#{precent_non_stop_words}% of words are keywords"