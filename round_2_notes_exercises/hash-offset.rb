def wrong_hash(hash)
  new_hash = {}
  hash_values = hash.values
  i = 0
  while i < hash_values.length
    current_first_letter = hash_values[i][0].to_sym
    new_hash[current_first_letter] = hash_values[i]
    i += 1
  end
  return new_hash
end

puts wrong_hash({:a => "banana", :b => "cabbage", :c => "dental_floss"})
puts wrong_hash({})