def double_nums(nums)
  doubled_nums = nums.map {|num| num * 2}
  return doubled_nums
end


def find_median(nums)
  sorted_nums = nums.sort
  if sorted_nums.count % 2 != 0
    return sorted_nums[sorted_nums.count / 2]
  else
    mid_num1 = sorted_nums[sorted_nums.count / 2]
    mid_num2 = sorted_nums[(sorted_nums.count / 2) - 1]
    average = (mid_num1 + mid_num2) / 2.0
    return average
  end
end


def perfect_square(range)
  squares = {}
  x = 1
  while x * x <= 100
    squares[x * x] = true
    x += 1
  end
  perfect_squares = range.select do |i|
    squares.has_key?(i) == true
  end
  return perfect_squares
end
  

#puts double_nums([1, 2, 3, 4])
#puts find_median([1, 2, 3, 4, 5, 6, 7])
#puts find_median([1, 2, 3, 4, 5, 6])
#puts find_median([4, 6, 2, 8, 5, 7])

puts perfect_square(1..100)
