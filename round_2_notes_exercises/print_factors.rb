def factors_of_num(num)
	factors = []
	(1..num).each do |potential_factor|
		factors << potential_factor if num % potential_factor == 0
	end
	return factors
end

(1..100).each {|num| print factors_of_num(num)}