#I am assuming that the string has no spaces.

def find_substring(range, string)
	return string[range]
end

def substrings(string)
	idx_of_first_letter = 0
	idx_of_final_letter = string.length - 1
	all_substrings = []
	while idx_of_first_letter < string.length
		(idx_of_first_letter..idx_of_final_letter).each do |variable_idx|
			all_substrings << (find_substring((idx_of_first_letter..variable_idx), string))
		end
		idx_of_first_letter += 1
	end
	return all_substrings
end

puts substrings("penguins")