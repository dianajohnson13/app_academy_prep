def two_sum(nums)
  i = 0
  while i < nums.length
    i2 = i + 1
    while i2 < nums.length
      if nums[i] + nums[i2] == 0
        return true
      end
      i2 += 1
    end
    i += 1
  end
  return false
end

puts two_sum([2, 5, 6, -5, 4])
puts two_sum([2, 3, 5, 6, 3])
puts two_sum([0, 4, 0])