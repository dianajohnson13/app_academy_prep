def get_computer_hand
	options = ["rock", "paper", "scissors"]
	computer_hand = options.sample
end

def determine_winner(user_hand, computer_hand)
	return "draw" if user_hand == computer_hand
	return	"user" if (user_hand == "paper") && (computer_hand == "rock")
	return	"user" if (user_hand == "rock") && (computer_hand == "scissors")
	return	"user" if (user_hand == "scissors") && (computer_hand == "paper")
	return	"computer"
end

def rps(user_hand)
	computer_hand = get_computer_hand
	winner = determine_winner(user_hand, computer_hand)
	return "I chose #{computer_hand}. You win!" if winner == "user"
	return "I chose #{computer_hand}. You lose!" if winner == "computer"
	return "I chose #{computer_hand}. It's a draw!" if winner == "draw"
end

puts "Hello. My name is Computer. I want to play Rock, Paper, Scissors!!"
puts "To play with me, please type in your choice: rock, paper, or scissors."
user_hand = gets.chomp
puts rps(user_hand)
puts "That was fun! Thank you for playing!"