def group_by_gender(original_couples)
	men = []
	women = []
	original_couples.each {|couple| men << [couple[0]]}
	original_couples.each {|couple| women << [couple[1]]}
	return [men, women]
end

def mix_and_match(men_and_women)
	men = men_and_women[0]
	women = men_and_women[1].shuffle
	new_couples = []
	men.each_with_index {|man, index| new_couples << [man, women[index]]}
	return new_couples
end

original_couples = [["Clyde", "Bonnie"], ["Jim", "Jane"], ["Romeo", "Juliet"]]
men_and_women = group_by_gender(original_couples)
new_couples = mix_and_match(men_and_women)
new_couples.each {|couple| puts couple}