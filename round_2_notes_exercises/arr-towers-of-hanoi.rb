
def get_move_request(rod1, rod2, rod3)
  initial_rod = ""
  while (initial_rod != "rod1") && (initial_rod != "rod2") && (initial_rod != "rod3")
    puts "Which rod would you like to take a disk from: rod1, rod2, or rod3?"
    initial_rod = gets.chomp
  end
  if initial_rod == "rod1"
    initial_rod = rod1
  elsif initial_rod == "rod2"
    initial_rod = rod2
  elsif initial_rod == "rod3"
    initial_rod = rod3
  end
  second_rod = ""
  while (second_rod != "rod1") && (second_rod != "rod2") && (second_rod != "rod3")
    puts "Which rod would you like to move it to?"
    second_rod = gets.chomp
  end
  if second_rod == "rod1"
    second_rod = rod1
  elsif second_rod == "rod2"
    second_rod = rod2
  elsif second_rod == "rod3"
    second_rod = rod3
  end
  return initial_rod, second_rod
end


def valid_move?(initial_rod, second_rod)
  if initial_rod.length == 0
    return false
  elsif (second_rod == []) || (initial_rod.last > second_rod.last)
    return true
  end
end


def initiate_move(initial_rod, second_rod)
    disk = initial_rod.pop
    second_rod << disk
end


def all_disks_moved?(rod2, rod3)
  if (rod2 == [1, 2, 3, 4]) || (rod3 == [1, 2, 3, 4])
    return true
  end
  return false
end



    
puts "Hello. Welcome to Diana's Tower's of Hanoi. 
Disks range in size from the largest being '1' to the smallest being '4'. 
The stack begins on rod 1."
puts ""
rod1 = [1, 2, 3, 4]
rod2 = []
rod3 = []
begin
  initial_rod, second_rod = get_move_request(rod1, rod2, rod3)
  if valid_move?(initial_rod, second_rod) == true
    initiate_move(initial_rod, second_rod)
  else
    puts "That move is not valid."
  end
  puts " "
  puts "Here are your new disk alignments."
  puts "Rod1:"
  puts rod1
  puts ""
  puts "Rod2:"
  puts rod2
  puts ""
  puts "Rod3:"
  puts rod3
  puts ""
end until all_disks_moved?(rod2, rod3) == true
puts "You did it!"