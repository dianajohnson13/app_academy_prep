def set_add_el(hash, element)
  if hash.has_key?(element) != true
    new_hash = {element => true}
    new_hash.merge!(hash)
  else
    return hash
  end
  return new_hash
end


def set_remove_el(hash, element)
  if hash.has_key?(element)
    new_hash = hash
    new_hash.delete(element)
  else
    return hash
  end
  return new_hash
end


def set_list_elements(hash)
  hash.keys
end


def set_member?(hash, element)
  hash[element]
end


def set_union(hash1, hash2)
  hash1.merge(hash2)
end


def set_intersection(hash1, hash2)
  hash1_keys = hash1.keys
  hash2_keys = hash2.keys
  intersecting_keys = []
  i = 0
  while i < hash1_keys.length
    j = 0
    while j < hash2_keys.length
      if hash1_keys[i] == hash2_keys[j]
        intersecting_keys << hash1_keys[i]
      end
      j += 1
    end
    i += 1
  end
  return intersecting_keys
end


def set_minus(hash1, hash2)
  hash1_keys = hash1.keys
  uncommon_keys = []
  i = 0
  while i < hash1_keys.length
    if hash2.has_key?(hash1_keys[i]) != true
      uncommon_keys << hash1_keys[i]
    end
    i += 1
  end
  return uncommon_keys
end

#puts set_remove_el({:love => true, :hate => false}, :good_things)
#puts set_remove_el({:love => true, :hate => false, :good_things => "pineapple"}, :good_things)

#puts set_list_elements({:love => true, :hate => false, :good_things => "pineapple"})

#puts set_member?({:love => true, :hate => false, :good_things => "pineapple"}, :good_things)

#puts set_union({:love => "good", :happiness => "good"}, {:hate => "bad", :anger => "bad"})

#puts set_intersection({:love => "good", :happiness => "good", :pineapple => "tasty", :anger => "bad"}, {:hate => "bad", :anger => "bad", :pineapple => "tasty"})

#puts set_minus({:love => "good", :happiness => "good", :pineapple => "tasty", :anger => "bad"}, {:hate => "bad", :anger => "bad", :pineapple => "tasty"})
