def my_uniq(array)
    new_array = []
    array.each do |x|
      if new_array.include?(x) == false
        new_array << x
      end
    end
    return new_array
end

puts my_uniq([1, 2, 1, 3, 3])
puts ""
puts my_uniq([1, 1, 1, 1, 1, 1])
