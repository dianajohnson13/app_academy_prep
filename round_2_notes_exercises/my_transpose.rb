def pretty_print(columns)
  i = 0
  while i < columns.length
    puts columns[i].to_s
    i += 1
  end
end

def my_transpose(rows)
  i = 0
  columns = []
  while i < rows[0].length
    j = 0
    current_column = []
    while j < rows.length
      current_column << rows[j][i]
      j += 1
    end
    columns << current_column
    i += 1
  end
  return pretty_print(columns)
end



puts my_transpose([[0, 1, 2], [3, 4, 5], [6, 7, 8], [4, 5, 7]])
puts my_transpose([[0, 1, 2, 9], [3, 4, 5, 4], [6, 7, 8, 2]])
puts my_transpose([[0, 1, 2], [3, 4, 5], [6, 7, 8]])